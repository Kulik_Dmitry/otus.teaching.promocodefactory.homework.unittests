﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Providers;
using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Providers
{
    public class CurrentDateTimeProvider
        : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}
