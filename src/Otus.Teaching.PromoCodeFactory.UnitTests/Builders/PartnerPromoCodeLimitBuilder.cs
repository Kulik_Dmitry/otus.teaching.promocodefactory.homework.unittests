﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerPromoCodeLimitBuilder
    {
        private PartnerPromoCodeLimit _partnerPromoCodeLimit;

        public PartnerPromoCodeLimitBuilder()
        {
            _partnerPromoCodeLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("c9bef066-3c5a-4e5d-9cff-bd54479f075e"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 12, 9),
                Limit = 100
            };
        }

        public PartnerPromoCodeLimitBuilder IsCanceled
        {
            get
            {
                _partnerPromoCodeLimit.CancelDate = new DateTime(2020, 8, 9);
                return this;
            }
        }

        public PartnerPromoCodeLimit Build()
        {
            return _partnerPromoCodeLimit;
        }        
    }
}
