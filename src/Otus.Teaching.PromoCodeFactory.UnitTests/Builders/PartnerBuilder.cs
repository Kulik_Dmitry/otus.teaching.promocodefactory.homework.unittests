﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerBuilder
    {
        private Partner _partner;

        public PartnerBuilder()
        {
            _partner = new Partner()
            {
                Id = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43"),
                Name = "Рыба твоей мечты",
                IsActive = false,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimitBuilder().Build()
                }
            };
        }
        public PartnerBuilder WithoutLimits
        {
            get
            {
                _partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
                return this;
            }
        }

        public PartnerBuilder WithNotActiveLimit
        {
            get
            {
                _partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
                _partner.PartnerLimits.Add(new PartnerPromoCodeLimitBuilder().IsCanceled.Build());
                return this;
            }
        }

        public PartnerBuilder IsActive
        {
            get
            {
                _partner.IsActive = true;
                return this;
            }
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}
