﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using FluentAssertions;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Providers;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersControllerMock;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProviderMock;

        private SetPartnerPromoCodeLimitRequest Request => new SetPartnerPromoCodeLimitRequest();

        public SetPartnerPromoCodeLimitAsyncTests()
        {   
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _currentDateTimeProviderMock = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnersControllerMock = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_NotFoundError()
        {
            var partnerId = Guid.NewGuid();
            Partner partner = null;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, Request);
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_BadRequestError()
        {
            var partnerId = Guid.NewGuid();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(new PartnerBuilder().Build());

            var expected = _partnersControllerMock.BadRequest("Данный партнер не активен");
            var result = await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, Request);
            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_RequestHasNotLimit_LimitLessThanZero()
        {
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().IsActive.Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var expected = _partnersControllerMock.BadRequest("Лимит должен быть больше 0");
            var result = await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, Request);
            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_ResetNumberIssuedPromoCodes()
        {
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().IsActive.Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var request = new SetPartnerPromoCodeLimitRequest() { Limit = 5 };

            await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, request);
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasNotActiveLimit_NotResetNumberIssuedPromoCodes()
        {
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().IsActive.WithNotActiveLimit.Build();            
            int numberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var request = new SetPartnerPromoCodeLimitRequest() { Limit = 5 };

            await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, request);
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_SetCancelDateNow()
        {
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().IsActive.Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var request = new SetPartnerPromoCodeLimitRequest() { Limit = 5 };
            var targetLimit = partner.PartnerLimits.First();

            var now = new DateTime(2020, 10, 14);
            var expectedLimit = new PartnerPromoCodeLimitBuilder().Build();
            expectedLimit.CancelDate = now;
            _currentDateTimeProviderMock.Setup(x => x.CurrentDateTime).Returns(now);

            await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, request);

            targetLimit.Should().BeEquivalentTo(expectedLimit);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CreateNewLimit_LimitSaved()
        {
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().IsActive.WithoutLimits.Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            int limit = 5;
            DateTime endDate = new DateTime(2020, 10, 14);
            var request = new SetPartnerPromoCodeLimitRequest() { Limit = limit, EndDate = endDate };

            DateTime nowDate = new DateTime(2020, 10, 14);
            _currentDateTimeProviderMock.Setup(x => x.CurrentDateTime).Returns(nowDate);

            PartnerPromoCodeLimit expect = new PartnerPromoCodeLimit()
            {
                CancelDate = null,
                CreateDate = nowDate,
                EndDate = endDate,
                Limit = limit,
                Partner = partner,
                PartnerId = partner.Id
            };

            await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, request);
            partner.PartnerLimits.FirstOrDefault().Should().BeEquivalentTo(expect);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CreateNewLimit_NewLimitSavedAndPreviousLimitClosed()
        {
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().IsActive.Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            int limit = 5;
            DateTime endDate = new DateTime(2020, 10, 14);
            DateTime nowDate = new DateTime(2020, 10, 14);

            var request = new SetPartnerPromoCodeLimitRequest() { Limit = limit, EndDate = endDate };
            _currentDateTimeProviderMock.Setup(x => x.CurrentDateTime).Returns(nowDate);

            PartnerPromoCodeLimit expectedPreviousLimit = new PartnerPromoCodeLimitBuilder().Build();
            expectedPreviousLimit.CancelDate = nowDate;

            PartnerPromoCodeLimit expectedNewLimit = new PartnerPromoCodeLimit()
            {
                CancelDate = null,
                CreateDate = nowDate,
                EndDate = endDate,
                Limit = limit,
                Partner = partner,
                PartnerId = partner.Id
            };

            await _partnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, request);
            partner.PartnerLimits.Should().BeEquivalentTo(new List<PartnerPromoCodeLimit> { expectedPreviousLimit, expectedNewLimit });
        }
    }
}